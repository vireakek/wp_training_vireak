let mix = require('laravel-mix');
mix
  .js('web/app/themes/wp_training/resources/js/app.js', 'web/app/themes/wp_training/build/')
  .sass('web/app/themes/wp_training/resources/scss/app.scss', 'web/app/themes/wp_training/build/')